package uz.pdp.mvdspravkademo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.mvdspravkademo.service.CheckCriminalService;

@RestController
@RequestMapping("/api/v1/checkCriminal")
@RequiredArgsConstructor
public class CheckCriminalController {

    private final CheckCriminalService checkCriminalService;

    @GetMapping()
    public boolean checkCriminal(
            @RequestParam String passportSerial,
            @RequestParam int passportNumber
    ){
        return checkCriminalService.checkCitizenCriminal(passportSerial,passportNumber);
    }
}
