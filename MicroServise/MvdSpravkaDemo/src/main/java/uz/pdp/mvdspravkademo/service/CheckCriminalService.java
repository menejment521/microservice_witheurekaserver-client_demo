package uz.pdp.mvdspravkademo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.mvdspravkademo.entity.Citizen;
import uz.pdp.mvdspravkademo.repo.CitizenRepo;

@Service
@RequiredArgsConstructor
public class CheckCriminalService {
    private final CitizenRepo citizenRepo;

    public boolean checkCitizenCriminal(String passportSerial, int passportNumber){
        Citizen citizen = citizenRepo.findByPassportSerialAndPassportNumber(passportSerial, passportNumber);
        return citizen.isCriminal();
    }
}
