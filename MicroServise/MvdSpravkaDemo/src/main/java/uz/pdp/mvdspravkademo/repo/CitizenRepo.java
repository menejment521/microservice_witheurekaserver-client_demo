package uz.pdp.mvdspravkademo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mvdspravkademo.entity.Citizen;

public interface CitizenRepo extends JpaRepository<Citizen, Long> {

    Citizen findByPassportSerialAndPassportNumber(String passportSerial, int passportNumber);

}
