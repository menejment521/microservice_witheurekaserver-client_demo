package uz.pdp.xodimlar.JsonHolder;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uz.pdp.xodimlar.config.FoignClien;

@FeignClient(name = "MVD",configuration = FoignClien.class)
public interface JsonPlaceHolder {

//    @RequestMapping(method = RequestMethod.GET)
//    Boolean checkCriminal(@RequestParam String passportSerial,
//                          @RequestParam int passportNumber);


    @GetMapping("/api/v1/checkCriminal")
    boolean check(
            @RequestParam String passportSerial,
            @RequestParam int passportNumber
    );

//    @RequestMapping(method = RequestMethod.GET, value = "/posts/{postId}", produces = "application/json")
//    Post getPostById(@PathVariable("postId") Long postId);
}
