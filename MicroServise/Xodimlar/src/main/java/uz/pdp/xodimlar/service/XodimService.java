package uz.pdp.xodimlar.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import uz.pdp.xodimlar.JsonHolder.JsonPlaceHolder;
import uz.pdp.xodimlar.entity.Xodim;
import uz.pdp.xodimlar.payload.XodimDto;
import uz.pdp.xodimlar.repo.XodimRepo;

@Service
@RequiredArgsConstructor
public class XodimService {

    private final XodimRepo xodimRepo;
    private final JsonPlaceHolder jsonPlaceHolder;
//    private final RestTemplate restTemplate;

    @Transactional
    public void saveXodim(XodimDto dto){
        //todo validate email and passport info
        //todo check MVD
//        Boolean criminal = restTemplate.getForObject(
//                "http://MVD1/api/v1/checkCriminal?passportSerial="
//                        + dto.getPassportSerial() +
//                        "&passportNumber=" + dto.getPassportNumber(),
//                Boolean.class);
        boolean criminal = jsonPlaceHolder.check(dto.getPassportSerial(), dto.getPassportNumber());
        if(criminal){
            throw new IllegalStateException("This employee is dangarious");
        }
        //todo save DB
        Xodim xodim = Xodim.builder()
                .fio(dto.getFio())
                .email(dto.getEmail())
                .passportSerial(dto.getPassportSerial())
                .passportNumber(dto.getPassportNumber())
                .build();
        xodimRepo.saveAndFlush(xodim);
        //todo send notification
    }
}
