package uz.pdp.xodimlar.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.xodimlar.entity.Xodim;

public interface XodimRepo extends JpaRepository<Xodim, Long> {
}
