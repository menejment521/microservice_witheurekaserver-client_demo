package uz.pdp.xodimlar.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Xodim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fio;
    private String email;
    private String passportSerial;
    private int passportNumber;

    public Xodim(String fio, String email, String passportSerial, int passportNumber) {
        this.fio = fio;
        this.email = email;
        this.passportSerial = passportSerial;
        this.passportNumber = passportNumber;
    }
}
