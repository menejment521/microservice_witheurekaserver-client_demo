package uz.pdp.xodimlar.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.xodimlar.payload.XodimDto;
import uz.pdp.xodimlar.service.XodimService;

@RestController
@RequestMapping("/api/v1/xodim")
@RequiredArgsConstructor
public class XodimControlller {

    private final XodimService xodimService;

    @PostMapping("/save")
    public HttpEntity<?> saveXodim(@RequestBody XodimDto xodimDto){
        xodimService.saveXodim(xodimDto);
        return ResponseEntity.status(201).body("Saved");    }
}
