package uz.pdp.xodimlar.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class XodimDto {
    private Long id;
    private String fio;
    private String email;
    private String passportSerial;
    private int passportNumber;
}
